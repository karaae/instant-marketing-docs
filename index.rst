**********************
Direct Mail for Eloqua
**********************

Direct mail: personalized, printed, and posted!
.. in this guide::
 ** In This Guide **    
 How does Direct Mail for Eloqua work?
 Personalizing the Printed Artwork
 URL shortener for personal tracked links
  
Direct Mail Intro
#################
Get direct mail printed and posted (worldwide) for you, from your Eloqua campaigns!

Here's how it works:

To start, you place our Direct Mail action onto your Campaign Canvas.

..image:: https://justaddfeatures.com/img/post-1.png
    :width: 200px
    :align: center
    :alt: Direct mail intro

You can then click on it to setup the message and artwork which will be printed.

The direct mail is printed and then sent to the contact based upon their Address, City and Country fields.



Printing and Posting Details
############################
The Direct Mail connector currently sends A5 postcards. One side is full-colour artwork, and one side is a message + address.

Both sides (the message and address) can be personalized with field-merges and more.

Cost per postcard: the price for the actual printing + postage (excluding our software) is £0.44/$0.62 per postcard sent to a UK address, and £0.73/$1.03 to anywhere else in the World.

Please note: if you want a high volume in another country, we may be able to integrate with a local supplier. Please contact us.



Getting Started with Direct Mail
################################
To install the Direct Mail connector please follow the installation guide.

We will need help you with initial setup. Please contact us so we can get you onboarded!


To use Direct Mail for Eloqua, place the Direct Mail action onto your Eloqua Campaign Canvas:



Then right-click it and click 'configure' to edit your message:




Artwork
*******
In the 'Artwork LP Url' field, please enter the URL of an Eloqua landing page.

This page will be personalized for the contact, and then converted into an image, and sent to the printers!


Why start from a landing page? This allows for highly personalized artwork. For example, you could include the contact's name in the artwork by simply having a field-merge on the landing page.
...Or you could use Dynamic Content to adjust your images or message, or you could even include the contact's company logo.

Basically - personalizing the artwork like this allows for some seriously creative campaigns.



Message
*******
Put whatever message you would like printed on the reverse of the postcard into the message box.

You can insert field-merges into this message. Just select any contact field from the 'Insert Field Merge' dropdown.


Personalized + Shortened URLs
*****************************
You can use the 'Insert LP Link URL' field above the message area to automatically generate a short URL
.
We will generate a tracking URL which is unique to that contact. This lets us track opens, and it also lets you personalize the landing-page.

This URL is sent via. a URL shortener so it is ultra-short and so that the tracked link is less obvious.




Test With PDF Previews
######################
Once you have customized your message, click 'Test' on the right hand side.

This will generate a personalized PDF preview for every test-contact.
To add or remove test-contacts just use the "+" or "-" buttons next to them.

Unlimited previews: When you run tests, you are generating a preview PDF only, and your mail will not be printed (and you will not be charged).



To see the PDF preview, just click the link. Here is an example of what the PDF preview looks like:

